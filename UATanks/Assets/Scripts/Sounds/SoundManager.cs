﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public AudioSource music;
    public AudioSource button;
    public Slider musicSlider;
    public Slider sfxSlider;
    float musicVolume;
    float sfxVolume;

    // Set mixer volumes to saved volumes
    private void Start()
    {
        music = GetComponent<AudioSource>();
        musicSlider.value = PlayerPrefs.GetFloat("musicSlider");
        sfxSlider.value = PlayerPrefs.GetFloat("sfxSlider");
    }
    private void Update()
    {
        PlayerPrefs.SetFloat("musicSlider", musicSlider.value);
        PlayerPrefs.SetFloat("sfxSlider", sfxSlider.value);
    }

    // function to set volume for exposed music volume parameter
    public void setMusicVolume(float volume)
    {
        musicVolume = volume;
        PlayerPrefs.Save();
    }

    // function to set volume for exposed sfx volume parameter
    public void setSFXVolume(float volume)
    {
        sfxVolume = volume;
        PlayerPrefs.Save();
    }
}