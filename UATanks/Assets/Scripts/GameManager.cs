﻿//Norman Nguyen
//Game Manager: Where it manages your important variables like scores and players, but didn't used it for this milestone yet.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public List<TankController> player;

    public TankData playerData;
    //Prefabs
    public int numPlayers;
    public GameObject playerPrefab;
    private TankHealth tankHealth;
    //How many AI tanks
    public int numAITanks;
    //Enemy Prefab
    public List<GameObject> enemyPrefabs;
    //Cameras
    public Camera P1Camera;
    public Camera P2Camera;

    public Text p1Lives;
    public Text p2Lives;
    //Volume
    public float sfxVolume;
    public float musicVolume;
    public int highScore;
    //Lives
    public int player1Lives = 3;
    public int player2Lives = 3;

    public bool singleplayer;
    public bool multiplayer;
    // Use this for initialization 
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            {
                Debug.LogError("ERROR: There can be only be one GameManager.");
                Destroy(gameObject);
            }
        }
        highScore = PlayerPrefs.GetInt("HighScore", 0);

    }
    //Start
    void Start()
    {
        //Start to the creation of the player tank and AI
        makePlayerTank();
        makeEnemyTank();
        p1Lives = GameObject.Find("P1Lives").GetComponent<Text>();
        p2Lives = GameObject.Find("P2Lives").GetComponent<Text>();
        playerData = GetComponent<TankData>();
    }

    void Update()
    {
        //LivesUI();
        //    if (tankHealth.currentHealth <= 0)
        //    {
        //        respawnPlayer();
        //    }
    }
    //Make Player Tank
    public void makePlayerTank()
    {
        //Generate the Tank
        //Vector 3 with Random Range where the tank will spawn randomly within that range
        for (int numTanks = 0; numTanks < numPlayers; numTanks++)
        {
            Vector3 position = new Vector3(Random.Range(-60.0f, 60.0f), -5, Random.Range(-60.0f, 60.0f));
            GameObject playerSpawnObject = Instantiate(playerPrefab, position, Quaternion.identity) as GameObject;
            playerSpawnObject.name = "Tank("+ numPlayers + ")";
            playerSpawnObject.transform.parent = this.transform;

            if (singleplayer)
            {
                SingleCamera();
                P1Camera.enabled = true;
                P2Camera.enabled = false;
                P2Camera.gameObject.SetActive(false);
            }
            else if (multiplayer)
            {
                numPlayers = 2;
                P1Camera.enabled = true;
                P2Camera.enabled = true;
                SplitScreen();
            }
        }
    }
    //Random Enemy Prefab
    public GameObject RandomEnemyPrefab()
    {
        return enemyPrefabs[Random.Range(0, enemyPrefabs.Count)];
    }
    //Make AI Tanks
    public void makeEnemyTank()
    {
        //For Loop
        for (int enemyTanks = 0; enemyTanks < numAITanks; enemyTanks++)
        {
            //Vector3 Position for the tank ranging withint -60 to 60 in X and Z
            Vector3 position = new Vector3(Random.Range(-60.0f, 60.0f), -5, Random.Range(-60.0f, 60.0f));
            //Spawn the object based on the enemies listed
            GameObject enemySpawnedObject = Instantiate(RandomEnemyPrefab(), position, Quaternion.identity) as GameObject;
            enemySpawnedObject.transform.parent = this.transform;
            //Name of the Enemy Tank
            enemySpawnedObject.name = "Enemy Tank ("+enemyTanks+")";
        }    
    }
    public void setMusicVolume(float volume)
    {
        musicVolume = volume;
    }

    // function to set volume for exposed sfx volume parameter
    public void setSFXVolume(float volume)
    {
        sfxVolume = volume;
    }
    public void ResetGame()
    {

    }
    //Set Single Player
    public void SinglePlayerOn()
    {
        singleplayer = true;
        SingleCamera();
    }
    //Set Multiplayer
    public void MultiplayerOn()
    {
        multiplayer = true;
        SplitScreen();
    }
    //Set Single Player off
    public void SinglePlayerOff()
    {
        singleplayer = false;
    }
    //Set Multiplayer off
    public void MultiplayerOff()
    {
        multiplayer = false;
    }
    //SingleCamera
    void SingleCamera()
    {
        if (P1Camera != null)
        {
            P1Camera.rect = new Rect(0, 0, 1, 1);
        }
    }
    //SplitScreen
    void SplitScreen()
    {
        if (P1Camera != null)
        {
            P1Camera.rect = new Rect(0, 0, .5f, 1);
        }
        if (P2Camera != null)
        {
            P2Camera.rect = new Rect(.5f, 0, .5f, 1);
        }
    }
    //GameObject Spawn
    GameObject respawnPlayer()
    {
        LivesUI();
        player1Lives--;
        Vector3 position = new Vector3(Random.Range(-60.0f, 60.0f), -5, Random.Range(-60.0f, 60.0f));
        GameObject playerRespawn = Instantiate(playerPrefab, position , Quaternion.identity) as GameObject;
        playerRespawn.name = "Tank";
        return playerRespawn;
    }
    void LivesUI()
    {
        p1Lives.text = "Lives: " + player1Lives.ToString();
        p2Lives.text = "Lives: " + player2Lives.ToString();
    }
    void GameOver()
    {
       
    }
}
