﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public Text scoreText;
    public Text highScoreText;
    public TankData data;

    public int highScore = 0;
	// Use this for initialization
	void Start ()
	{
	    scoreText = GetComponent<Text>();
	    data = GetComponent<TankData>();
        scoreText = GameObject.Find("P1Score").GetComponent<Text>();
	    highScoreText = GameObject.Find("HighScoreText").GetComponent<Text>();
        setUIScore();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    setUIScore();
	}

    private void setUIScore()
    {
        scoreText.text = "Score: " + data.currentScore.ToString();
        highScoreText.text = "High Score: " + data.currentScore.ToString();
    }
}
