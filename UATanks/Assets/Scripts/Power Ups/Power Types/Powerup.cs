﻿//Norman Nguyen
//Main Powerup class
[System.Serializable]
public class Powerup
{
    //Powerup Enum
    public enum PowerupType
    {
        HealthPowerup,
        RapidFirePowerup,
        SpeedPowerup
    }
    //Countdown timer
    public float countdown;
    //Permanent
    public bool isPerm;

    //Constructor
    public Powerup() { }

    public PowerupType powerupType;
    //Override function for three powerups
    public virtual void OnAddPowerup(TankData data) { }
    public virtual void OnRemovePowerup(TankData data) { }
}